package br.com.mvsouza.testerest;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author Matheus Souza
 */
public class Main {

    private final static int PORTA = 9998;
    private final static String IP = "http://0.0.0.0/";

    public static void main(String[] args) {
        JdkHttpServerFactory.createHttpServer(UriBuilder.fromUri(IP).port(PORTA).build(), new ResourceConfig(SensoriamentoWS.class));
        System.out.println(String.format("Servidor iniciado em %s:%d", IP, PORTA));
    }

}
