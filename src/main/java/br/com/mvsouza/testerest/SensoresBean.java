package br.com.mvsouza.testerest;

/**
 *
 * @author Matheus Souza
 */
public class SensoresBean {

    private double temperatura;
    private double umidade;
    private double ph;

    public SensoresBean() {
    }

    public SensoresBean(double temperatura, double umidade, double ph) {
        this.temperatura = temperatura;
        this.umidade = umidade;
        this.ph = ph;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getUmidade() {
        return umidade;
    }

    public void setUmidade(double umidade) {
        this.umidade = umidade;
    }

    public double getPh() {
        return ph;
    }

    public void setPh(double ph) {
        this.ph = ph;
    }

    @Override
    public String toString() {
        return String.format("Temperatura: %.2f - Umidade: %.2f - PH: %.2f", temperatura, umidade, ph);
    }

}
