package br.com.mvsouza.testerest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matheus Souza
 */
@Path("sensoriamento")
public class SensoriamentoWS {

    public SensoriamentoWS() {
        super();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response salvaTemperatura(@Context Request request, SensoresBean json) {
        return Response.status(Response.Status.CREATED).entity(json.toString()).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaTemperaturaByData(@Context Request request) {
        return Response.status(Response.Status.OK).entity(new SensoresBean(26, 58, 6)).build();
    }

}
